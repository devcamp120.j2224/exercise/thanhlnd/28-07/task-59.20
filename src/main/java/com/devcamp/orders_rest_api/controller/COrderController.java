package com.devcamp.orders_rest_api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orders_rest_api.model.Orders;
import com.devcamp.orders_rest_api.repository.OrderRepository;


@RestController
public class COrderController {
    @Autowired
    OrderRepository orderRepository;

    @CrossOrigin
    @GetMapping("/orders")
    public ResponseEntity<List<Orders>> getListOrder(){
        try {
            List<Orders> listOrder = new ArrayList<>();
            orderRepository.findAll().forEach(listOrder::add);

            return new ResponseEntity<List<Orders>>(listOrder, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
